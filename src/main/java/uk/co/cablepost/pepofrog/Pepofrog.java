package uk.co.cablepost.pepofrog;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import software.bernie.geckolib3.GeckoLib;
import uk.co.cablepost.pepofrog.entity.PepofrogEntity;

public class Pepofrog implements ModInitializer {

    public static String MOD_ID = "pepofrog";

    public static final EntityType<PepofrogEntity> PEPOFROG_ENTITY = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(MOD_ID, "pepofrog"),
            FabricEntityTypeBuilder.create(SpawnGroup.CREATURE, PepofrogEntity::new).dimensions(EntityDimensions.fixed(0.25f, 0.75f)).build()
    );



    @Override
    public void onInitialize() {
        GeckoLib.initialize();

        FabricDefaultAttributeRegistry.register(PEPOFROG_ENTITY, PepofrogEntity.createMobAttributes());
    }
}
