package uk.co.cablepost.pepofrog.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import software.bernie.example.registry.EntityRegistry;
import uk.co.cablepost.pepofrog.Pepofrog;
import uk.co.cablepost.pepofrog.entity.PepofrogEntity;
import uk.co.cablepost.pepofrog.entity_renderer.PepofrogEntityRenderer;

@Environment(EnvType.CLIENT)
public class PepofrogClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        EntityRendererRegistry.register(Pepofrog.PEPOFROG_ENTITY, PepofrogEntityRenderer::new);
    }
}
