package uk.co.cablepost.pepofrog.model;

import net.minecraft.util.Identifier;
import software.bernie.geckolib3.GeckoLib;
import software.bernie.geckolib3.model.AnimatedGeoModel;
import uk.co.cablepost.pepofrog.Pepofrog;
import uk.co.cablepost.pepofrog.entity.PepofrogEntity;

public class PepofrogModel extends AnimatedGeoModel<PepofrogEntity> {
    @Override
    public Identifier getModelLocation(PepofrogEntity object) {
        return new Identifier(Pepofrog.MOD_ID, "geo/pepofrog.geo.json");
    }

    @Override
    public Identifier getTextureLocation(PepofrogEntity object) {
        return new Identifier(Pepofrog.MOD_ID, "textures/entity/pepofrog.png");
    }

    @Override
    public Identifier getAnimationFileLocation(PepofrogEntity animatable) {
        return new Identifier(Pepofrog.MOD_ID, "animations/pepofrog.animation.json");
    }
}
