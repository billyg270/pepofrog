package uk.co.cablepost.pepofrog.entity_renderer;

import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.scoreboard.ScoreboardCriterion;
import net.minecraft.util.Identifier;
import software.bernie.geckolib3.model.AnimatedGeoModel;
import software.bernie.geckolib3.renderers.geo.GeoEntityRenderer;
import uk.co.cablepost.pepofrog.entity.PepofrogEntity;
import uk.co.cablepost.pepofrog.model.PepofrogModel;

public class PepofrogEntityRenderer extends GeoEntityRenderer<PepofrogEntity> {
    public PepofrogEntityRenderer(EntityRendererFactory.Context ctx, AnimatedGeoModel<PepofrogEntity> modelProvider) {
        super(ctx, modelProvider);
    }

    public PepofrogEntityRenderer(EntityRendererFactory.Context ctx) {
        super(ctx, new PepofrogModel());
    }
}
