{
	"format_version": "1.12.0",
	"minecraft:geometry": [
		{
			"description": {
				"identifier": "geometry.pepofrog",
				"texture_width": 64,
				"texture_height": 64,
				"visible_bounds_width": 2,
				"visible_bounds_height": 2.5,
				"visible_bounds_offset": [0, 0.75, 0]
			},
			"bones": [
				{
					"name": "root",
					"pivot": [0, 0, 0]
				},
				{
					"name": "body",
					"parent": "root",
					"pivot": [0, 0, 0],
					"cubes": [
						{"origin": [-3, 2, -3], "size": [6, 6, 6], "uv": [0, 0]}
					]
				},
				{
					"name": "head",
					"parent": "body",
					"pivot": [0, 8, 0],
					"cubes": [
						{"origin": [-3, 8, -3], "size": [6, 5, 6], "uv": [0, 15]}
					]
				},
				{
					"name": "lips",
					"parent": "head",
					"pivot": [0, 8, 0],
					"cubes": [
						{"origin": [-3.5, 8, -2.9], "size": [1, 1, 5.8], "uv": [0, 28]}
					]
				},
				{
					"name": "left_eye",
					"parent": "head",
					"pivot": [-3, 11, 2],
					"cubes": [
						{"origin": [-3.25, 10, 0.9], "size": [1, 2, 2], "uv": [0, 0]}
					]
				},
				{
					"name": "right_eye",
					"parent": "head",
					"pivot": [0, 8, -2],
					"cubes": [
						{"origin": [-3.25, 10, -2.9], "size": [1, 2, 2], "uv": [30, 1]}
					]
				}
			]
		}
	]
}